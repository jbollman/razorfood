using System.Collections.Generic;
using System.Linq;
using RazorFood.Core;

namespace RazorFood.Data
{
    public interface IRestaurantData
    {
        IEnumerable<Restaurant> GetRestaurantsByName(string name);
        Restaurant GetById(int id);
    }
    public class InMemoryRestaurantData : IRestaurantData
    {
        readonly List<Restaurant> restaurants;
        
        
        public InMemoryRestaurantData()
        {
            restaurants = new List<Restaurant>()
            {
                new Restaurant { Id = 1, Name = "JoJo Pizza", Location = "denver", Cuisine = CuisineType.Italian},
                new Restaurant { Id = 2, Name = "JoJo Mexican", Location = "mexico", Cuisine = CuisineType.Mexican},
                new Restaurant { Id = 3, Name = "JoJo Indian", Location = "denver", Cuisine = CuisineType.Indian},
            };

        }

        public Restaurant GetById(int id)
        {
            return restaurants.SingleOrDefault(r => r.Id == id);
        }
        public IEnumerable<Restaurant> GetRestaurantsByName(string name = null)
        {
            return from r in restaurants
                where string.IsNullOrEmpty(name) || r.Name.StartsWith(name)
                orderby r.Name
                select r;
        }
    }
}