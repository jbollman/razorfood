using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RazorFood.Core;
using RazorFood.Data;

namespace RazorFood.Pages.Restaurants
{
    public class Detail : PageModel
    {
        public Restaurant Restaurant {get; set; }
        public readonly IRestaurantData restaurantData;

        public Detail(IRestaurantData restaurantData)
        {
            this.restaurantData = restaurantData;
        }
        public IActionResult OnGet(int restaurantId)
        {
            Restaurant = restaurantData.GetById(restaurantId);
            if (Restaurant == null)
            {
                return RedirectToPage("./NotFound");
            }
            return Page();
        }
    }
}